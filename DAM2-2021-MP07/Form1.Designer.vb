﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.resultat = New System.Windows.Forms.Label()
        Me.boto_0 = New System.Windows.Forms.Button()
        Me.boto_1 = New System.Windows.Forms.Button()
        Me.boto_9 = New System.Windows.Forms.Button()
        Me.boto_8 = New System.Windows.Forms.Button()
        Me.boto_7 = New System.Windows.Forms.Button()
        Me.boto_6 = New System.Windows.Forms.Button()
        Me.boto_5 = New System.Windows.Forms.Button()
        Me.boto_4 = New System.Windows.Forms.Button()
        Me.boto_3 = New System.Windows.Forms.Button()
        Me.boto_2 = New System.Windows.Forms.Button()
        Me.res = New System.Windows.Forms.Label()
        Me.valor = New System.Windows.Forms.TextBox()
        Me.multiplicacio = New System.Windows.Forms.Button()
        Me.divisio = New System.Windows.Forms.Button()
        Me.resta = New System.Windows.Forms.Button()
        Me.suma = New System.Windows.Forms.Button()
        Me.igual = New System.Windows.Forms.Button()
        Me.resultats = New System.Windows.Forms.Button()
        Me.moviment = New System.Windows.Forms.PictureBox()
        CType(Me.moviment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'resultat
        '
        Me.resultat.AutoSize = True
        Me.resultat.Location = New System.Drawing.Point(422, 9)
        Me.resultat.Name = "resultat"
        Me.resultat.Size = New System.Drawing.Size(0, 13)
        Me.resultat.TabIndex = 1
        '
        'boto_0
        '
        Me.boto_0.Location = New System.Drawing.Point(45, 291)
        Me.boto_0.Name = "boto_0"
        Me.boto_0.Size = New System.Drawing.Size(162, 39)
        Me.boto_0.TabIndex = 2
        Me.boto_0.Text = "0"
        Me.boto_0.UseVisualStyleBackColor = True
        '
        'boto_1
        '
        Me.boto_1.Location = New System.Drawing.Point(45, 235)
        Me.boto_1.Name = "boto_1"
        Me.boto_1.Size = New System.Drawing.Size(50, 50)
        Me.boto_1.TabIndex = 3
        Me.boto_1.Text = "1"
        Me.boto_1.UseVisualStyleBackColor = True
        '
        'boto_9
        '
        Me.boto_9.Location = New System.Drawing.Point(157, 123)
        Me.boto_9.Name = "boto_9"
        Me.boto_9.Size = New System.Drawing.Size(50, 50)
        Me.boto_9.TabIndex = 4
        Me.boto_9.Text = "9"
        Me.boto_9.UseVisualStyleBackColor = True
        '
        'boto_8
        '
        Me.boto_8.Location = New System.Drawing.Point(101, 123)
        Me.boto_8.Name = "boto_8"
        Me.boto_8.Size = New System.Drawing.Size(50, 50)
        Me.boto_8.TabIndex = 5
        Me.boto_8.Text = "8"
        Me.boto_8.UseVisualStyleBackColor = True
        '
        'boto_7
        '
        Me.boto_7.Location = New System.Drawing.Point(45, 123)
        Me.boto_7.Name = "boto_7"
        Me.boto_7.Size = New System.Drawing.Size(50, 50)
        Me.boto_7.TabIndex = 6
        Me.boto_7.Text = "7"
        Me.boto_7.UseVisualStyleBackColor = True
        '
        'boto_6
        '
        Me.boto_6.Location = New System.Drawing.Point(157, 179)
        Me.boto_6.Name = "boto_6"
        Me.boto_6.Size = New System.Drawing.Size(50, 50)
        Me.boto_6.TabIndex = 7
        Me.boto_6.Text = "6"
        Me.boto_6.UseVisualStyleBackColor = True
        '
        'boto_5
        '
        Me.boto_5.Location = New System.Drawing.Point(101, 179)
        Me.boto_5.Name = "boto_5"
        Me.boto_5.Size = New System.Drawing.Size(50, 50)
        Me.boto_5.TabIndex = 8
        Me.boto_5.Text = "5"
        Me.boto_5.UseVisualStyleBackColor = True
        '
        'boto_4
        '
        Me.boto_4.Location = New System.Drawing.Point(45, 179)
        Me.boto_4.Name = "boto_4"
        Me.boto_4.Size = New System.Drawing.Size(50, 50)
        Me.boto_4.TabIndex = 9
        Me.boto_4.Text = "4"
        Me.boto_4.UseVisualStyleBackColor = True
        '
        'boto_3
        '
        Me.boto_3.Location = New System.Drawing.Point(157, 235)
        Me.boto_3.Name = "boto_3"
        Me.boto_3.Size = New System.Drawing.Size(50, 50)
        Me.boto_3.TabIndex = 10
        Me.boto_3.Text = "3"
        Me.boto_3.UseVisualStyleBackColor = True
        '
        'boto_2
        '
        Me.boto_2.Location = New System.Drawing.Point(101, 235)
        Me.boto_2.Name = "boto_2"
        Me.boto_2.Size = New System.Drawing.Size(50, 50)
        Me.boto_2.TabIndex = 11
        Me.boto_2.Text = "2"
        Me.boto_2.UseVisualStyleBackColor = True
        '
        'res
        '
        Me.res.AutoSize = True
        Me.res.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.res.Location = New System.Drawing.Point(42, 20)
        Me.res.Name = "res"
        Me.res.Size = New System.Drawing.Size(133, 26)
        Me.res.TabIndex = 12
        Me.res.Text = "RESULTAT"
        '
        'valor
        '
        Me.valor.Location = New System.Drawing.Point(45, 76)
        Me.valor.Name = "valor"
        Me.valor.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.valor.Size = New System.Drawing.Size(161, 20)
        Me.valor.TabIndex = 13
        '
        'multiplicacio
        '
        Me.multiplicacio.Location = New System.Drawing.Point(213, 123)
        Me.multiplicacio.Name = "multiplicacio"
        Me.multiplicacio.Size = New System.Drawing.Size(50, 50)
        Me.multiplicacio.TabIndex = 14
        Me.multiplicacio.Text = "X"
        Me.multiplicacio.UseVisualStyleBackColor = True
        '
        'divisio
        '
        Me.divisio.Location = New System.Drawing.Point(213, 179)
        Me.divisio.Name = "divisio"
        Me.divisio.Size = New System.Drawing.Size(50, 50)
        Me.divisio.TabIndex = 15
        Me.divisio.Text = "/"
        Me.divisio.UseVisualStyleBackColor = True
        '
        'resta
        '
        Me.resta.Location = New System.Drawing.Point(213, 235)
        Me.resta.Name = "resta"
        Me.resta.Size = New System.Drawing.Size(50, 50)
        Me.resta.TabIndex = 16
        Me.resta.Text = "-"
        Me.resta.UseVisualStyleBackColor = True
        '
        'suma
        '
        Me.suma.Location = New System.Drawing.Point(213, 291)
        Me.suma.Name = "suma"
        Me.suma.Size = New System.Drawing.Size(50, 39)
        Me.suma.TabIndex = 17
        Me.suma.Text = "+"
        Me.suma.UseVisualStyleBackColor = True
        '
        'igual
        '
        Me.igual.Location = New System.Drawing.Point(212, 67)
        Me.igual.Name = "igual"
        Me.igual.Size = New System.Drawing.Size(50, 50)
        Me.igual.TabIndex = 18
        Me.igual.Text = "="
        Me.igual.UseVisualStyleBackColor = True
        '
        'resultats
        '
        Me.resultats.Location = New System.Drawing.Point(212, 10)
        Me.resultats.Name = "resultats"
        Me.resultats.Size = New System.Drawing.Size(50, 50)
        Me.resultats.TabIndex = 19
        Me.resultats.Text = "res"
        Me.resultats.UseVisualStyleBackColor = True
        '
        'moviment
        '
        Me.moviment.Image = CType(resources.GetObject("moviment.Image"), System.Drawing.Image)
        Me.moviment.Location = New System.Drawing.Point(312, 6)
        Me.moviment.Name = "moviment"
        Me.moviment.Size = New System.Drawing.Size(204, 111)
        Me.moviment.TabIndex = 20
        Me.moviment.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(548, 353)
        Me.Controls.Add(Me.moviment)
        Me.Controls.Add(Me.resultats)
        Me.Controls.Add(Me.igual)
        Me.Controls.Add(Me.suma)
        Me.Controls.Add(Me.resta)
        Me.Controls.Add(Me.divisio)
        Me.Controls.Add(Me.multiplicacio)
        Me.Controls.Add(Me.valor)
        Me.Controls.Add(Me.res)
        Me.Controls.Add(Me.boto_2)
        Me.Controls.Add(Me.boto_3)
        Me.Controls.Add(Me.boto_4)
        Me.Controls.Add(Me.boto_5)
        Me.Controls.Add(Me.boto_6)
        Me.Controls.Add(Me.boto_7)
        Me.Controls.Add(Me.boto_8)
        Me.Controls.Add(Me.boto_9)
        Me.Controls.Add(Me.boto_1)
        Me.Controls.Add(Me.boto_0)
        Me.Controls.Add(Me.resultat)
        Me.Name = "Form1"
        Me.Text = "Calculadora"
        CType(Me.moviment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents resultat As Label
    Friend WithEvents boto_0 As Button
    Friend WithEvents boto_1 As Button
    Friend WithEvents boto_9 As Button
    Friend WithEvents boto_8 As Button
    Friend WithEvents boto_7 As Button
    Friend WithEvents boto_6 As Button
    Friend WithEvents boto_5 As Button
    Friend WithEvents boto_4 As Button
    Friend WithEvents boto_3 As Button
    Friend WithEvents boto_2 As Button
    Friend WithEvents res As Label
    Friend WithEvents valor As TextBox
    Friend WithEvents multiplicacio As Button
    Friend WithEvents divisio As Button
    Friend WithEvents resta As Button
    Friend WithEvents suma As Button
    Friend WithEvents igual As Button
    Friend WithEvents resultats As Button
    Friend WithEvents moviment As PictureBox
End Class
