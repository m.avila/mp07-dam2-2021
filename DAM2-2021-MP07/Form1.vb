﻿Public Class Form1

    Dim valor1 As Int32 'creem la variable pel primer valor
    Dim valor2 As Int32 'creem la variable pel segon valor
    Dim operador As String 'creem la variable per saber quin operador fem servir
    Dim resultat_final(10) As Int32
    Dim i As Int32

    Dim posX As Int32
    Dim posY As Int32
    Dim em_moc As Boolean = False

    Dim moc_imatge As Boolean = False

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        res.Text = "Aquí hi haurà el resultat" 'mostra el text a l'etiqueta del resultat
        i = 0
    End Sub

    Private Sub boto_0_Click(sender As Object, e As EventArgs) Handles boto_0.Click
        afegirElValorAlTextBox(0) 'afegeix el valor 0 al textBox dels valors
    End Sub

    Private Sub boto_1_Click(sender As Object, e As EventArgs) Handles boto_1.Click
        afegirElValorAlTextBox(1) 'afegeix el valor 1 al textBox dels valors
    End Sub

    Private Sub boto_2_Click(sender As Object, e As EventArgs) Handles boto_2.Click
        afegirElValorAlTextBox(2) 'afegeix el valor 2 al textBox dels valors
    End Sub
    Private Sub boto_3_Click(sender As Object, e As EventArgs) Handles boto_3.Click
        afegirElValorAlTextBox(3) 'afegeix el valor 3 al textBox dels valors
    End Sub

    Private Sub boto_4_Click(sender As Object, e As EventArgs) Handles boto_4.Click
        afegirElValorAlTextBox(4) 'afegeix el valor 4 al textBox dels valors
    End Sub

    Private Sub boto_5_Click(sender As Object, e As EventArgs) Handles boto_5.Click
        afegirElValorAlTextBox(5) 'afegeix el valor 5 al textBox dels valors
    End Sub

    Private Sub boto_6_Click(sender As Object, e As EventArgs) Handles boto_6.Click
        afegirElValorAlTextBox(6) 'afegeix el valor 6 al textBox dels valors
    End Sub

    Private Sub boto_7_Click(sender As Object, e As EventArgs) Handles boto_7.Click
        afegirElValorAlTextBox(7) 'afegeix el valor 7 al textBox dels valors
    End Sub

    Private Sub boto_8_Click(sender As Object, e As EventArgs) Handles boto_8.Click
        afegirElValorAlTextBox(8) 'afegeix el valor 8 al textBox dels valors
    End Sub

    Private Sub boto_9_Click(sender As Object, e As EventArgs) Handles boto_9.Click
        afegirElValorAlTextBox(9) 'afegeix el valor 9 al textBox dels valors
    End Sub
    Public Sub afegirElValorAlTextBox(ByVal text As String)
        valor.Text = valor.Text + text 'afegeix el valor -text- al textBox dels valors
    End Sub

    Private Sub suma_Click(sender As Object, e As EventArgs) Handles suma.Click
        valor1 = passarAEnter(valor.Text) 'desa el valor a la variable valor1
        primerOperand(" + ") 'ens mostra a l'etiqueta el valor i el símbol +
        netejarTextBoxValor() 'neteja el textBox on introduim valors
        operador = "suma"
    End Sub

    Private Sub resta_Click(sender As Object, e As EventArgs) Handles resta.Click
        valor1 = passarAEnter(valor.Text) 'desa el valor a la variable valor1
        primerOperand(" - ") 'ens mostra a l'etiqueta el valor i el símbol +
        netejarTextBoxValor() 'neteja el textBox on introduim valors
        operador = "resta"
    End Sub
    Private Sub multiplicacio_Click(sender As Object, e As EventArgs) Handles multiplicacio.Click
        valor1 = passarAEnter(valor.Text) 'desa el valor a la variable valor1
        primerOperand(" * ") 'ens mostra a l'etiqueta el valor i el símbol +
        netejarTextBoxValor() 'neteja el textBox on introduim valors
        operador = "multiplicacio"
    End Sub
    Private Sub divisio_Click(sender As Object, e As EventArgs) Handles divisio.Click
        valor1 = passarAEnter(valor.Text) 'desa el valor a la variable valor1
        primerOperand(" / ") 'ens mostra a l'etiqueta el valor i el símbol +
        netejarTextBoxValor() 'neteja el textBox on introduim valors
        operador = "divisio"
    End Sub

    Private Sub igual_Click(sender As Object, e As EventArgs) Handles igual.Click
        valor2 = passarAEnter(valor.Text) 'desa el valor a la variable valor2
        If operador = "suma" Then
            resultat_final(i) = resultatOperacions(valor1 + valor2) 'mostra el resultat de les operacions
        ElseIf operador = "resta" Then
            resultat_final(i) = resultatOperacions(valor1 - valor2) 'mostra el resultat de les operacions
        ElseIf operador = "divisio" Then
            resultat_final(i) = resultatOperacions(valor1 / valor2) 'mostra el resultat de les operacions
        ElseIf operador = "multiplicacio" Then
            resultat_final(i) = resultatOperacions(valor1 * valor2) 'mostra el resultat de les operacions
        End If
        operador = ""
        i = i + 1
        netejarTextBoxValor() 'neteja el textBox on introduim valors
        netejarVariables() 'desa el valor de les variables

    End Sub

    Private Sub netejarTextBoxValor()
        valor.Text = "" 'neteja el textBox on introduim valors
    End Sub

    Private Sub netejarVariables()
        valor1 = 0 'posa la variable valor1 a 0
        valor2 = 0 'posa la variable valor2 a 0
    End Sub
    Private Function passarAEnter(ByVal valor As String) As Int32
        Return Convert.ToInt32(valor) 'retorna el valor com enter
    End Function

    Private Sub primerOperand(ByVal operador As String)
        res.Text = Convert.ToString(valor1) + operador 'mostra el primer valor i l'operador
    End Sub
    Private Function resultatOperacions(ByVal resultat As Int32) As Int32
        res.Text = res.Text + Convert.ToString(valor2) + " = " + Convert.ToString(resultat) 'mostra els dos valors, l'operador i el resultat
        Return resultat 'retornem el resultat
    End Function

    Private Function mostrarResultat() As String 'bucle per mostrar tots els resultats
        Dim resultats As String
        For j = 0 To i - 1
            resultats = resultats + Convert.ToString(resultat_final(j)) + vbCrLf
        Next j

        'Altres opcions amb altres bucles

        'Dim j As Int32
        'j = 0
        'While j < i
        '    resultats = resultats + Convert.ToString(resultat_final(j)) + vbCrLf
        'End While

        'Do
        '    resultats = resultats + Convert.ToString(resultat_final(j)) + vbCrLf
        'Loop While j < i

        'Do
        '    resultats = resultats + Convert.ToString(resultat_final(j)) + vbCrLf
        'Loop Until j > i

        Return resultats

    End Function

    Private Sub resultats_Click(sender As Object, e As EventArgs) Handles resultats.Click
        Dim response As MsgBoxResult 'creem un quadre de missatges
        response = MsgBox(mostrarResultat(), MsgBoxStyle.Information, "Resultat") 'mostrem el quadre de missatges amb el resultat de l'operació
    End Sub

    Private Sub moviment_MouseDown(sender As Object, e As MouseEventArgs) Handles moviment.MouseDown
        em_moc = True
        posX = e.X
        posY = e.Y
    End Sub

    Private Sub moviment_MouseMove(sender As Object, e As MouseEventArgs) Handles moviment.MouseMove
        If em_moc = True Then
            moviment.Left = moviment.Left + (e.X - posX)
            moviment.Top = moviment.Top + (e.Y - posY)
        End If
    End Sub
    Private Sub moviment_MouseUp(sender As Object, e As MouseEventArgs) Handles moviment.MouseUp
        em_moc = False
    End Sub

    Private Sub moviment_Click(sender As Object, e As EventArgs) Handles moviment.Click
        moc_imatge = True
    End Sub

    Private Sub Form1_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick
        If moc_imatge = True Then
            moviment.Left = e.X
            moviment.Top = e.Y
        End If
        moc_imatge = False
    End Sub

End Class
